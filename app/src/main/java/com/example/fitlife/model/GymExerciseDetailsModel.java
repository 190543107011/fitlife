package com.example.fitlife.model;

import java.io.Serializable;

public class GymExerciseDetailsModel implements Serializable {


    int ExerciseslistId;
    int CategoryId;
    int SubCategoryId;

    public int getExerciseslistId() {
        return ExerciseslistId;
    }

    public void setExerciseslistId(int exerciseslistId) {
        ExerciseslistId = exerciseslistId;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    public int getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        SubCategoryId = subCategoryId;
    }

    public int getExerciseId() {
        return ExerciseId;
    }

    public void setExerciseId(int exerciseId) {
        ExerciseId = exerciseId;
    }

    public String getGifName() {
        return GifName;
    }

    public void setGifName(String gifName) {
        GifName = gifName;
    }

    public String getExersiseNameStapes() {
        return ExersiseNameStapes;
    }

    public void setExersiseNameStapes(String exersiseNameStapes) {
        ExersiseNameStapes = exersiseNameStapes;
    }

    public String getExercisesStapes() {
        return ExercisesStapes;
    }

    public void setExercisesStapes(String exercisesStapes) {
        ExercisesStapes = exercisesStapes;
    }

    int ExerciseId;
    String GifName;
    String ExersiseNameStapes;
    String ExercisesStapes;



}
