package com.example.fitlife.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.fitlife.R;
import com.example.fitlife.adapter.Gym_Exercise_Detail_Adapter;
import com.example.fitlife.database.Gym_Exercises_Details;
import com.example.fitlife.model.GymExerciseDetailsModel;

import java.util.ArrayList;

public class exercise_detail_activity extends AppCompatActivity {

   RecyclerView activity_gym_exercisesdetail_recycler_view;
   Gym_Exercise_Detail_Adapter exercise_detail_adapter;
   Gym_Exercises_Details gym_exercises_details;
   ArrayList<GymExerciseDetailsModel>exercisedetail;



    Intent intent;
    int exerciseslistId;
    String exercisesName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_detail_activity);
        init();
        process();
        listener();
    }

    public void init(){

        intent = getIntent();

        exerciseslistId = intent.getIntExtra("ExerciseslistId", 1);
       exercisesName = intent.getStringExtra("ExercisesName");


        activity_gym_exercisesdetail_recycler_view = findViewById(R.id.activity_gym_exercisesdetail_recycler_view);
        exercisedetail  = new ArrayList<>();
        gym_exercises_details  = new Gym_Exercises_Details(this);
        exercisedetail = gym_exercises_details.all_exercise_details(exerciseslistId);

    }

    public void process(){

        exercise_detail_adapter = new Gym_Exercise_Detail_Adapter(exercisedetail,this);

        activity_gym_exercisesdetail_recycler_view.setLayoutManager(new GridLayoutManager(this, 1));

        activity_gym_exercisesdetail_recycler_view.setAdapter(exercise_detail_adapter);


    }

    public  void listener(){

    }
}