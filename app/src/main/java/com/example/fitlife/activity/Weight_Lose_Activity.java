package com.example.fitlife.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.fitlife.R;
import com.example.fitlife.adapter.GymSubCategoryAdapter;
import com.example.fitlife.adapter.WeightloseAdapter;
import com.example.fitlife.database.GymSubCategoryDb;
import com.example.fitlife.database.Weightlose;
import com.example.fitlife.model.GymSubCategoryModel;
import com.example.fitlife.model.WeightloseModel;

import java.util.ArrayList;

public class Weight_Lose_Activity extends AppCompatActivity {

    GymSubCategoryAdapter gymSubCategoryAdapter;

    WeightloseAdapter weightloseAdapter;

    GymSubCategoryDb gymSubCategoryDb;
    Weightlose weightlose;

    ArrayList<GymSubCategoryModel> subcategorylist;
    ArrayList<WeightloseModel>weeklist;

    Intent intent;

    int categoryId;
    String categoryName;

    RecyclerView activity_weightlose_category_recycler_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight__lose_);

        init();
        process();
        listener();

    }

    public  void init(){

        intent = getIntent();

        categoryId = intent.getIntExtra("CategoryId", 2);
        categoryName = intent.getStringExtra("CategoryName");

        activity_weightlose_category_recycler_view = findViewById(R.id.activity_weightlose_category_recycler_view);


        subcategorylist = new ArrayList<>();
        gymSubCategoryDb = new GymSubCategoryDb(this);
        subcategorylist = gymSubCategoryDb.all_sub_category_list(categoryId);

        weeklist = new ArrayList<>();
        weightlose = new Weightlose(this);
   //     weeklist = weightlose.all_week_name();


    }

    public void process(){

       // gymSubCategoryAdapter = new GymSubCategoryAdapter(subcategorylist, this);
      //  weightloseAdapter = new WeightloseAdapter(weeklist,this);

        activity_weightlose_category_recycler_view.setLayoutManager(new GridLayoutManager(this, 1));

        activity_weightlose_category_recycler_view.setAdapter(gymSubCategoryAdapter);
        activity_weightlose_category_recycler_view.setAdapter(weightloseAdapter);


    }


    public void listener(){

    }
}