package com.example.fitlife.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.fitlife.R;
import com.example.fitlife.ToolBar_Activity;
import com.example.fitlife.adapter.GymCategoryAdapter;
import com.example.fitlife.database.GymCategory;
import com.example.fitlife.model.GymCategoryModel;

import java.util.ArrayList;

public class GymCategoryActivity extends ToolBar_Activity {

    GymCategoryAdapter gymCategoryAdapter;

    GymCategory gymCategory;

    ArrayList<GymCategoryModel> categorylist;

    RecyclerView activity_gym_category_recycler_view;

    CardView homeworkout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym_category);
        setUpActionBar(("Fit Life"), true);
        init();
        process();
        listener();
    }



    public void init(){

        /*txthomeworkout = findViewById(R.id.txthomeworkout);
        txtweightlose = findViewById(R.id.txtweightlose);
        txtleanbody = findViewById(R.id.txtleanbody);
        txtbullkbody = findViewById(R.id.txtbullkbody);
        txtdiet = findViewById(R.id.txtdiet);
        txtsixpack = findViewById(R.id.txtsixpack);*/

        activity_gym_category_recycler_view = findViewById(R.id.activity_gym_category_recycler_view);

        homeworkout = findViewById(R.id.homeworkout);


        categorylist = new ArrayList<>();

        gymCategory = new GymCategory(this);

        categorylist = gymCategory.all_category_list();


    }



    private void process(){

        gymCategoryAdapter = new GymCategoryAdapter(categorylist, this, new GymCategoryAdapter.ClickListeners() {
            @Override
            public void onViewClick(int position) {
                int tempCategoryId = categorylist.get(position).getCategoryId();
                String tempCategoryName = categorylist.get(position).getCategoryName();

                Intent intent = new Intent(GymCategoryActivity.this,GymSubCategoryActivity.class);
                intent.putExtra("CategoryId",tempCategoryId);
                intent.putExtra("CategoryName",tempCategoryName);
                startActivity(intent);

            }
        });
        activity_gym_category_recycler_view.setLayoutManager(new GridLayoutManager(this,2));
        activity_gym_category_recycler_view.setAdapter(gymCategoryAdapter);

    }


    public void listener(){



    }

}