package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.fitlife.model.GymSubCategoryModel;

import java.util.ArrayList;

public class GymSubCategoryDb extends Mydatabase {

    public static final String GymSubCategory = "GymSubCategory";
    public static final String SubCategoryId = "SubCategoryId";
    public static final String SubCategoryName = "SubCategoryName";
    public static final String SubCategoryLogo = "Logo";
    public static final String Col_CategoryId = "CategoryId";

    public ArrayList<GymSubCategoryModel> all_sub_category_list(int id) {

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<GymSubCategoryModel> subcategorylist = new ArrayList<>();

        String query = "select * from " + GymSubCategory+" where "+Col_CategoryId+"="+id;


        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        do {

            GymSubCategoryModel gymSubCategoryModel = new GymSubCategoryModel();
            gymSubCategoryModel.setSubCategoryId(cursor.getInt(cursor.getColumnIndex(SubCategoryId)));
            gymSubCategoryModel.setSubCategoryName(cursor.getString(cursor.getColumnIndex(SubCategoryName)));
            gymSubCategoryModel.setLogo(cursor.getString(cursor.getColumnIndex(SubCategoryLogo)));
            gymSubCategoryModel.setCategoryId(cursor.getInt(cursor.getColumnIndex(Col_CategoryId)));

            subcategorylist.add(gymSubCategoryModel);

        } while (cursor.moveToNext());

        db.close();
        cursor.close();
        return subcategorylist;

    }


    public GymSubCategoryDb(Context context) {
        super(context);
    }
}
