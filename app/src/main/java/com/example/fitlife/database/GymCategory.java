package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.model.GymCategoryModel;

import java.util.ArrayList;

public class GymCategory extends Mydatabase {

    public static final String GymCategory = "GymCategory";
    public static final String Gym_Category_Id = "CategoryId";
    public static final String Gym_Category_Name = "CategoryName";
    public static final String Gym_Logo = "Logo";

    public ArrayList<GymCategoryModel> all_category_list() {

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<GymCategoryModel> CategoryList = new ArrayList<>();

        String query = "select * from " + GymCategory;

        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        do {

            GymCategoryModel gymCategoryModel = new GymCategoryModel();
            gymCategoryModel.setCategoryId(cursor.getInt(cursor.getColumnIndex(Gym_Category_Id)));
            gymCategoryModel.setCategoryName(cursor.getString(cursor.getColumnIndex(Gym_Category_Name)));
            gymCategoryModel.setLogo(cursor.getString(cursor.getColumnIndex(Gym_Logo)));

            CategoryList.add(gymCategoryModel);

        } while (cursor.moveToNext());
        db.close();

        cursor.close();

        return CategoryList;

    }


    public GymCategory(Context context) {
        super(context);
    }
}
