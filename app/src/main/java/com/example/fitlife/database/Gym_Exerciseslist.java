package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.model.GymExercisesListModel;

import java.util.ArrayList;

import static com.example.fitlife.database.GymSubCategoryDb.GymSubCategory;

public class Gym_Exerciseslist extends Mydatabase {

public  static  final String GymExercisesList = "GymExercisesList";
    public  static  final String ExerciseslistId = "ExerciseslistId";
    public  static  final String ExercisesName = "ExercisesName";
    public  static  final String ExercisesImage = "ExercisesImage";
    public  static  final String CategoryId = "CategoryId";
    public  static  final String SubCategoryId = "SubCategoryId";

    public ArrayList<GymExercisesListModel>all_exercise_list(int id){

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<GymExercisesListModel> exerciseslist = new ArrayList<>();

        String query = "select * from " + GymExercisesList+" where "+SubCategoryId+"="+id;

        Cursor cursor = db.rawQuery(query,null);

        cursor.moveToFirst();
        do{
            GymExercisesListModel gymExercisesListModel = new GymExercisesListModel();
            gymExercisesListModel.setExerciseslistId(cursor.getInt(cursor.getColumnIndex(ExerciseslistId)));
            gymExercisesListModel.setExercisesName(cursor.getString(cursor.getColumnIndex(ExercisesName)));
            gymExercisesListModel.setExercisesImage(cursor.getString(cursor.getColumnIndex(ExercisesImage)));
            gymExercisesListModel.setSubCategoryId(cursor.getInt(cursor.getColumnIndex(SubCategoryId)));


            exerciseslist.add(gymExercisesListModel);
        }while (cursor.moveToNext());

        db.close();
        cursor.close();
        return exerciseslist;
    }


    public Gym_Exerciseslist(Context context) {
        super(context);
    }
}
