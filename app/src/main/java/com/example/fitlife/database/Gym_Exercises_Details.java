package com.example.fitlife.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.fitlife.model.GymExerciseDetailsModel;

import java.util.ArrayList;

public class Gym_Exercises_Details extends Mydatabase {

public static final String ExerciseDetails = "ExerciseDetails";
public static final String ExerciseId = "ExerciseId";
public static final String GifName ="GifName";
    public static final String ExersiseNameStapes = "ExersiseNameStapes";
    public static final String ExercisesStapes = "ExercisesStapes";
    public static final String ExerciseslistId ="ExerciseslistId";
    public  static  final String CategoryId = "CategoryId";
    public  static  final String SubCategoryId = "SubCategoryId";

public ArrayList<GymExerciseDetailsModel>all_exercise_details(int id){

    SQLiteDatabase db = getReadableDatabase();
    ArrayList<GymExerciseDetailsModel>exercisedetail = new ArrayList<>();
    String query = "select * from " +ExerciseDetails+" where "+ExerciseslistId+"="+id;

    Cursor cursor = db.rawQuery(query,null);
    cursor.moveToFirst();

    do{

        GymExerciseDetailsModel  gymExerciseDetailsModel = new GymExerciseDetailsModel();
        gymExerciseDetailsModel.setExerciseId(cursor.getInt(cursor.getColumnIndex(ExerciseId)));
        gymExerciseDetailsModel.setGifName(cursor.getString(cursor.getColumnIndex(GifName)));
        gymExerciseDetailsModel.setExercisesStapes(cursor.getString(cursor.getColumnIndex(ExersiseNameStapes)));
        gymExerciseDetailsModel.setExercisesStapes(cursor.getString(cursor.getColumnIndex(ExercisesStapes)));
        gymExerciseDetailsModel.setCategoryId(cursor.getInt(cursor.getColumnIndex(SubCategoryId)));

        exercisedetail.add(gymExerciseDetailsModel);

    }while (cursor.moveToNext());

    db.close();
    cursor.close();
    return exercisedetail;
}

    public Gym_Exercises_Details(Context context) {
        super(context);
    }
}
