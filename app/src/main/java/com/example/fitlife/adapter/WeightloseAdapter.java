package com.example.fitlife.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.R;
import com.example.fitlife.model.WeightloseModel;

import java.util.ArrayList;

public class WeightloseAdapter extends RecyclerView.Adapter<WeightloseAdapter.ViewHolder> {
    ArrayList<WeightloseModel>weeklist;
    Activity context;

  ClickListeners clickListeners;

    public interface ClickListeners{
        void onViewClick(int position);
    }

    public WeightloseAdapter(ArrayList<WeightloseModel> weeklist, Activity context,ClickListeners onViewClick) {
        this.weeklist = weeklist;
        this.context = context;
        clickListeners = onViewClick;

    }


    @NonNull
    @Override
    public WeightloseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_weightlose,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull WeightloseAdapter.ViewHolder holder, int position) {

       holder.row_weekname.setText(String.valueOf(weeklist.get(position).getWeekName()));
       holder.row_dayname.setText(String.valueOf(weeklist.get(position).getSubCategoryId()));
       holder.row_weightlose.setText(String.valueOf(weeklist.get(position).getWeekId()));


    }

    @Override
    public int getItemCount() {
        return weeklist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView row_weekname,row_dayname,row_weightlose;
        CardView row_gymdayname;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            row_weekname = itemView.findViewById(R.id.row_weekname);
            row_dayname = itemView.findViewById(R.id.row_dayname);
            row_weightlose = itemView.findViewById(R.id.row_weightlose);
            row_gymdayname = itemView.findViewById(R.id.row_gymdayname);

        }
    }
}
