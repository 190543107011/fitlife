package com.example.fitlife.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.R;
import com.example.fitlife.model.GymExerciseDetailsModel;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;

public class Gym_Exercise_Detail_Adapter extends RecyclerView.Adapter<Gym_Exercise_Detail_Adapter.ViewHolder> {
    ArrayList<GymExerciseDetailsModel>exercisedetail;

    public Gym_Exercise_Detail_Adapter(ArrayList<GymExerciseDetailsModel> exercisedetail, Activity context) {
        this.exercisedetail = exercisedetail;
        this.context = context;
    }

    Activity context;


    @NonNull
    @Override
    public Gym_Exercise_Detail_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Gym_Exercise_Detail_Adapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_exercises_details,parent,false)) ;
    }

    @Override
    public void onBindViewHolder(@NonNull Gym_Exercise_Detail_Adapter.ViewHolder holder, int position) {

        int id = context.getResources().getIdentifier("com.example.fitlife:drawable/" + exercisedetail.get(position).getGifName(),null, null);
        holder.row_gif.setImageResource(id);

        holder.row_exercisesteps.setText(String.valueOf(exercisedetail.get(position).getExersiseNameStapes()));
        holder.row_exercisedetail.setText(String.valueOf(exercisedetail.get(position).getExercisesStapes()));
    }

    @Override
    public int getItemCount() {
        return exercisedetail.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView row_exercisesteps,row_exercisedetail;
        GifImageView row_gif;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            row_exercisedetail = itemView.findViewById(R.id.row_exercisedetail);
            row_exercisesteps = itemView.findViewById(R.id.row_exercisesteps);
            row_gif = itemView.findViewById(R.id.row_gif);
        }
    }
}
