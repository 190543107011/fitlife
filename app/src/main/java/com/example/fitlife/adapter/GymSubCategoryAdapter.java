package com.example.fitlife.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.R;
import com.example.fitlife.model.GymSubCategoryModel;

import java.util.ArrayList;

public class GymSubCategoryAdapter extends RecyclerView.Adapter<GymSubCategoryAdapter.ViewHolder> {
    ArrayList<GymSubCategoryModel>subcategorylist;
    Activity context;
    ViewHolder holder;
   ClickListeners clickListeners;

    public interface ClickListeners{
        void onViewClick(int position);
    }


    public GymSubCategoryAdapter(ArrayList<GymSubCategoryModel> subcategorylist, Activity  context,ClickListeners onViewClick) {
        this.subcategorylist = subcategorylist;
        this.context = context;
        clickListeners = onViewClick;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_gymsubcategory,parent,false)) ;
    }

    @Override
    public void onBindViewHolder(@NonNull GymSubCategoryAdapter.ViewHolder holder, final int position) {

        holder.row_gymsubcategoryid.setText(String.valueOf(subcategorylist.get(position).getSubCategoryId()));
        holder.row_gymsubcategoryname.setText(String.valueOf(subcategorylist.get(position).getSubCategoryName()));

        int id = context.getResources().getIdentifier("com.example.fitlife:drawable/" + subcategorylist.get(position).getLogo(),null, null);
        holder.row_gymsubcategorylogo.setImageResource(id);

        holder.chest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListeners.onViewClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return subcategorylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView row_gymsubcategoryname,row_gymsubcategoryid;
        ImageView row_gymsubcategorylogo;
        CardView chest;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            row_gymsubcategoryid = itemView.findViewById(R.id.row_gymsubcategoryid);
            row_gymsubcategoryname = itemView.findViewById(R.id.row_gymsubcategoryname);
            row_gymsubcategorylogo = itemView.findViewById(R.id.row_gymsubcategorylogo);

            chest = itemView.findViewById(R.id.chest);



        }
    }
}
