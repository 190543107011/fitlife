package com.example.fitlife.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fitlife.R;
import com.example.fitlife.model.GymExercisesListModel;

import java.util.ArrayList;

public class GymExerciseslistAdapter extends RecyclerView.Adapter<GymExerciseslistAdapter.ViewHolder> {

    ArrayList<GymExercisesListModel>exerciseslist;
    Activity context;
    ClickListeners clickListeners;

    public interface ClickListeners{
        void onViewClick(int position);
    }

    public GymExerciseslistAdapter(ArrayList<GymExercisesListModel> exerciseslist, Activity context,ClickListeners onViewClick) {
        this.exerciseslist = exerciseslist;
        this.context = context;
        clickListeners = onViewClick;
    }
    @NonNull
    @Override
    public GymExerciseslistAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_exercises_category,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull GymExerciseslistAdapter.ViewHolder holder, final int position) {

        holder.row_gymexerciselistid.setText(String.valueOf(exerciseslist.get(position).getExerciseslistId()));
        holder.row_gymexercisename.setText(String.valueOf(exerciseslist.get(position).getExercisesName()));

        int id = context.getResources().getIdentifier("com.example.fitlife:drawable/" + exerciseslist.get(position).getExercisesImage(),null, null);
        holder.row_gymexerciselogo.setImageResource(id);

        holder.pushps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListeners.onViewClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return exerciseslist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView row_gymexercisename,row_gymexerciselistid;
        ImageView row_gymexerciselogo;
        CardView pushps;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            row_gymexerciselistid = itemView.findViewById(R.id.row_gymexerciselistid);
            row_gymexercisename = itemView.findViewById(R.id.row_gymexercisename);
            row_gymexerciselogo = itemView.findViewById(R.id.row_gymexerciselogo);
            pushps = itemView.findViewById(R.id.pushps);

        }
    }
}
