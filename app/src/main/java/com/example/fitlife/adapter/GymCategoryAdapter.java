package com.example.fitlife.adapter;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fitlife.R;
import com.example.fitlife.activity.GymCategoryActivity;
import com.example.fitlife.activity.GymSubCategoryActivity;
import com.example.fitlife.model.GymCategoryModel;

import java.util.ArrayList;

public class GymCategoryAdapter extends RecyclerView.Adapter<GymCategoryAdapter.ViewHolder> {

    ArrayList<GymCategoryModel> categorylist;
    Activity context;

    ClickListeners clickListeners;

    public interface ClickListeners{
        void onViewClick(int position);
    }

    public GymCategoryAdapter(ArrayList<GymCategoryModel> categorylist, Activity context,ClickListeners onViewClick) {
        this.categorylist = categorylist;
        this.context = context;
        clickListeners = onViewClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_gymcategory, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.row_gymcategoryid.setText(String.valueOf(categorylist.get(position).getCategoryId()));
        holder.row_gymcategoryname.setText(String.valueOf(categorylist.get(position).getCategoryName()));

        int id = context.getResources().getIdentifier("com.example.fitlife:drawable/" + categorylist.get(position).getLogo(),null, null);
        holder.row_gymcategorylogo.setImageResource(id);

        holder.homeworkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListeners.onViewClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categorylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView row_gymcategoryid, row_gymcategoryname;
        ImageView row_gymcategorylogo;

        CardView homeworkout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            homeworkout = itemView.findViewById(R.id.homeworkout);

            row_gymcategoryid = itemView.findViewById(R.id.row_gymcategoryid);
            row_gymcategoryname = itemView.findViewById(R.id.row_gymcategoryname);
            row_gymcategorylogo = itemView.findViewById(R.id.row_gymcategorylogo);

        }
    }
}